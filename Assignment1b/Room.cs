﻿namespace Assignment1b
{
    public class Room
    {
        public int number;
        public int adultNumber;
        public int childNumber;
        public string type;
        public string bedType;

        public Room(int num, int adultNum, int childNum, string ty, string bedTy)
        {
            number = num;
            adultNumber = adultNum;
            childNumber = childNum;
            type = ty;
            bedType = bedTy;
        }
    }
}