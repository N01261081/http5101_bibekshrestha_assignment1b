﻿using System.Collections.Generic;

namespace Assignment1b
{
    public class Extra
    {
        public List<string> meal;
        public string petQuerry;
        public List<string> pet;

        public Extra(List<string> m, string petQ, List<string> p)
        {
            meal = m;
            petQuerry = petQ;
            pet = p;
        }
    }
}