﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Diagnostics;

namespace Assignment1b
{
    public partial class HotelBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Booking(object sender, EventArgs e)
        {
            //Checking to see if the inputs are valid
            if (!Page.IsValid)
            {
                return;
            }

            //defining variables needed to instantiate 'Days' class
            DateTime checkin = DateTime.ParseExact(daysCheckIn.Text, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            DateTime checkout = DateTime.ParseExact(daysCheckOut.Text, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            //Instantiating 'Days' object
            Days newDays = new Days(checkin, checkout);

            //defining variables needed to instantiate 'Client' class
            string name = clientName.Text.ToString();
            string address = clientAddress.Text.ToString();
            string phone = clientPhone.Text.ToString();
            string email = clientEmail.Text.ToString();
            //Instantiating 'Client' object
            Client newClient = new Client();
            newClient.ClientName = name;
            newClient.ClientAddress = address;
            newClient.ClientPhone = phone;
            newClient.ClientEmail = email;

            //defining variables needed to instantiate 'Room' class
            int number = Convert.ToInt32(roomNumber.SelectedValue);
            int adultNumber = Convert.ToInt32(roomAdultNumber.SelectedValue);
            int childNumber = Convert.ToInt32(roomChildrenNumber.SelectedValue);
            string type = roomType.SelectedValue;
            string bedType = roomBedType.SelectedValue.ToString();
            //Instantiating 'Room' object
            Room newRoom = new Room(number, adultNumber, childNumber, type, bedType);

            //defining variables needed to instantiate 'Extra' class
            List<string> meal = new List<string> { };
            string petQuerry = extraPetQuery.SelectedValue.ToString();
            List<string> pet = new List<string> { };
            //inserting values in list meal
            //Debug.WriteLine(extraMeal.Items.Count);
            foreach(ListItem i in extraMeal.Items)
            {
                if (i.Selected)
                {
                    meal.Add(i.Text);
                }
            }
            //Debug.WriteLine(meal.Count);


            //inserting values in list petkind
            foreach(ListItem i in extraPetKind.Items)
            {
                if (i.Selected)
                {
                    pet.Add(i.Text);
                }
            }
            //Debug.WriteLine(pet.Count);
            
            Extra newExtra = new Extra(meal, petQuerry, pet);

            Booking newBooking = new Booking(newDays, newClient, newRoom, newExtra);

            outputDiv.InnerHtml = newBooking.PrintBookingReview();



        }
    }
}