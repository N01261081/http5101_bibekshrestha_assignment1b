﻿using System;

namespace Assignment1b
{
    public class Booking
    {
        public Days days;
        public Client client;
        public Room room;
        public Extra extra;

        public Booking(Days D, Client C, Room R, Extra E)
        {
            days = D;
            client = C;
            room = R;
            extra = E;
        }

        public double CalculateBooking()
        {
            double totalPrice = 0;

            //calculate price for the room type
            if (room.type == "Deluxe Room")
            {
                totalPrice += 150;
            }
            else if (room.type == "Executive Room")
            {
                totalPrice += 200;
            }
            else if (room.type == "Premier Room")
            {
                totalPrice += 250;
            }
            else if (room.type == "One Bedroom Suite")
            {
                totalPrice += 300;
            }
            else if (room.type == "One Bedroom Deluxe Suite")
            {
                totalPrice += 350;
            }
            else if (room.type == "Speciality Suite")
            {
                totalPrice += 500;
            }

            //calculate the price for bedType
            //we don't add any extra price for twin bed
            if (room.bedType == "King")
            {
                totalPrice += 20;
            }

            //calculate the price for number of room
            totalPrice *= room.number;

            //calculate the price for extra meals
            //we only charge for extra meals ($5 per meal) to adults not children
            totalPrice += extra.meal.Count * room.adultNumber * 5;

            //calculating price for total days
            TimeSpan spanDays = days.checkout - days.checkin;
            int totalDays = Convert.ToInt32(spanDays.TotalDays);
            totalPrice *= totalDays;

            return totalPrice;
        }

        public string PrintBookingReview()
        {
            string review = "Your total amount for the booking is $" + CalculateBooking().ToString() + "<br><br>";
            review += "<strong>Your booking review</strong><br>";
            review += "Your name: " + client.ClientName.ToString() + "<br>";
            review += "Your address: " + client.ClientAddress.ToString() + "<br>";
            review += "Your phone: " + client.ClientPhone.ToString() + "<br>";
            review += "Your email: " + client.ClientEmail.ToString() + "<br>";
            review += "You selected " + room.number.ToString() + " " + room.type.ToString() + "(s) with " + room.bedType.ToString() + " bed(s)" + "<br>";
            review += "No of adult guest(s): " + room.adultNumber.ToString() + "<br>";
            if (room.childNumber > 0)
            {
                review += "No of child guest(s): " + room.childNumber.ToString() + "<br>";
            }
            if(extra.meal.Count > 0)
            {
                review += "Meal options choosen: " + String.Join(", ", extra.meal.ToArray()) + "<br>";
            }
            if (extra.petQuerry == "Yes")
            {
                review += "Your pet(s): " + String.Join(", ", extra.pet.ToArray());
            }




            return review;
        }
    }
}