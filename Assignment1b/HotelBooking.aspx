﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="Assignment1b.HotelBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml", lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <title>Hotel Booking Page</title>
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet" />
    <link href="Layout.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <main>
        <header>
            <h1>Book your room(s)</h1>
        </header>
        <form id="form1" runat="server" method="post" action="#">
            <section id="sectionCheckInOut">
                <div>
                    <asp:Label AssociatedControlID="daysCheckIn" Text="Check-in" runat="server"></asp:Label>
                    <asp:TextBox id="daysCheckIn" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="daysCheckIn" ID="validatorCheckIn" runat="server" 
                        ErrorMessage="Provide check-in date" CssClass="errorMsg"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label AssociatedControlID="daysCheckOut" Text="Check-out" runat="server"></asp:Label>
                    <asp:TextBox id="daysCheckOut" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="daysCheckOut" ID="validatorCheckOut" runat="server" 
                        ErrorMessage="Provide check-out date" CssClass="errorMsg"></asp:RequiredFieldValidator>
                </div>
            </section>
            <section>
                <div>
                    <asp:Label AssociatedControlID="clientName" Text="Full Name" runat="server"></asp:Label>
                    <asp:TextBox ID="clientName" runat="server" placeholder="eg: John Doe"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="clientName" ID="validatorClientName" runat="server" 
                        ErrorMessage="Provide your full name" CssClass="errorMsg"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label AssociatedControlID="clientAddress" Text="Address" runat="server"></asp:Label>
                    <asp:TextBox ID="clientAddress" runat="server" placeholder="eg: Toronto, ON"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="clientAddress" ID="validatorClientAddress" runat="server" 
                        ErrorMessage="Provide your address" CssClass="errorMsg"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label AssociatedControlID="clientPhone" Text="Phone" runat="server"></asp:Label>
                    <asp:TextBox ID="clientPhone" runat="server" placeholder="eg: 123-456-7890"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="clientPhone" ID="validatorClientPhone" runat="server" 
                        ErrorMessage="Provide your contact phone number" CssClass="errorMsg"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ControlToValidate="clientPhone" runat="server" 
                        ValidationExpression="^\d{3}-\d{3}-\d{4}$" ID="expValidatorClientPhone" 
                        ErrorMessage="Invalid phone number" CssClass="errorMsg"></asp:RegularExpressionValidator>
                </div>
                <div>
                    <asp:Label AssociatedControlID="clientEmail" Text="Email" runat="server"></asp:Label>
                    <asp:TextBox ID="clientEmail" runat="server" TextMode="Email" placeholder="eg: example@example.com" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="clientEmail" ID="validatorClientEmail" runat="server" 
                        ErrorMessage="Provide your contact email address" CssClass="errorMsg"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ControlToValidate="clientEmail" runat="server" 
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ID="expValidatorClientEmail" ErrorMessage="Invalid email address" CssClass="errorMsg"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ControlToValidate="clientEmail" runat="server" Type="String" 
                        Operator="NotEqual" ValueToCompare="bibekmanshrestha@gmail.com" 
                        ErrorMessage="You are the owner. You don't need to book a room!" CssClass="errorMsg"></asp:CompareValidator>
                </div>
            </section>
            <section>
                <div>
                <asp:Label AssociatedControlID="roomNumber" Text="Rooms" runat="server"></asp:Label>
                <asp:DropDownList ID="roomNumber" runat="server">
                    <asp:ListItem Enabled="true" Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="roomAdultNumber" Text="Adults" runat="server"></asp:Label>
                <asp:DropDownList ID="roomAdultNumber" runat="server">
                    <asp:ListItem Enabled="true" Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="roomChildrenNumber" Text="Children" runat="server"></asp:Label>
                <asp:DropDownList ID="roomChildrenNumber" runat="server">
                    <asp:ListItem Enabled="true" Text="0" Value="0"></asp:ListItem>
                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="roomType" Text="Type of room" runat="server"></asp:Label>
                <asp:DropDownList ID="roomType" runat="server">
                    <asp:ListItem Enabled="true" Text="Deluxe Room" Value="Deluxe Room"></asp:ListItem>
                    <asp:ListItem Text="Executive Room" Value="Executive Room"></asp:ListItem>
                    <asp:ListItem Text="Premier Room" Value="Premier Room"></asp:ListItem>
                    <asp:ListItem Text="One Bedroom Suite" Value="One Bedroom Suite"></asp:ListItem>
                    <asp:ListItem Text="One Bedroom Deluxe Suite" Value="One Bedroom Deluxe Suite"></asp:ListItem>
                    <asp:ListItem Text="Speciality Suite" Value="Speciality Suite"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="roomBedType" Text="Type of bed" runat="server"></asp:Label>
                <asp:RadioButtonList ID="roomBedType" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="King" Text="King Bed"/>
                    <asp:ListItem Value="Twin" Text="Twin Bed"/>
                </asp:RadioButtonList>
            </div>
            </section>
            <section>
                <div>
                <asp:Label AssociatedControlID="extraMeal" Text="Would you like to include any meal?" runat="server"></asp:Label>
                <asp:CheckBoxList ID="extraMeal" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Breakfast" Value="Breakfast" />
                    <asp:ListItem Text="Lunch" Value="Lunch" />
                    <asp:ListItem Text="Snacks" Value="Snacks" />
                </asp:CheckBoxList>
            </div>

            <div>
                <asp:Label AssociatedControlID="extraPetQuery" Text="Do you have a pet?" runat="server"></asp:Label>
                <asp:RadioButtonList ID="extraPetQuery" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Text="No" Value="No"/>
                    <asp:ListItem Text="Yes" Value="Yes"/>
                </asp:RadioButtonList>
            </div>
            <div>
                <asp:Label AssociatedControlID="extraPetKind" Text="If yes, What kind?" runat="server"></asp:Label>
                <asp:CheckBoxList ID="extraPetKind" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Dog" Value="Dog" />
                    <asp:ListItem Text="Cat" Value="Cat" />
                    <asp:ListItem Text="Other" Value="Other" />
                </asp:CheckBoxList>
            </div>
            </section>
            <section>
                <asp:Button runat="server" ID="submitButton" Text="Submit" OnClick="Booking"/>
                <asp:ValidationSummary runat="server" DisplayMode="List" EnableClientScript="true" ShowSummary="true" CssClass="errorMsg errorSummary"/>
            </section>

            <div id="outputDiv" runat="server"></div>
            <div id="test" runat="server"></div>
        </form>
    </main>
</body>
</html>
