﻿namespace Assignment1b
{
    public class Client
    {
        private string clientName;
        private string clientAddress;
        private string clientPhone;
        private string clientEmail;

        public Client()
        {
            
        }

        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }

        public string ClientAddress
        {
            get { return clientAddress; }
            set { clientAddress = value; }
        }

        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
    }
}