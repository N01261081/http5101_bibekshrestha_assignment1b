﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1b
{
    public class Days
    {
        //informations to be included in class 'Date'
        public DateTime checkin;
        public DateTime checkout;

        //Constructor function of the class
        public Days(DateTime i, DateTime o)
        {
            checkin = i;
            checkout = o;
        }
    }
}